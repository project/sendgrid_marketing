<?php

/**
 * Implements hook_rules_action_info().
 */
function sendgrid_marketing_rules_action_info() {
  $actions = array();

  $actions['sendgrid_marketing_subscribe_action'] = array(
    'label' => t('Subscribe email to SendGrid Marketing'),
    'type' => '*',
    'parameter' => array(
      'email' => array(
        'label' => t('Email', array(), array('context' => 'email to subscribe')),
        'type' => 'text',
        'optional' => FALSE,
        'default mode' => 'selector',
      ),
      'name' => array(
        'label' => t('Name', array(), array('context' => 'name of subscriber')),
        'type' => 'text',
        'default mode' => 'selector',
      ),
      'list' => array(
        'label' => t('List', array(), array('context' => 'list name')),
        'type' => 'text',
        'optional' => FALSE,
        'restriction' => 'input',
      ),
    ),
    'group' => t('SendGrid Marketing'),
  );

  return $actions;
}


/**
 * Action: Send an order to OMS for fulfilment.
 */
function sendgrid_marketing_subscribe_action($email, $name, $list, $context = array()) {
  $vars = array('%email' => $email, '%list' => $list);
  watchdog('sendgrid_marketing',
    'Subscribed %email to %list ...',
    $vars,
    WATCHDOG_INFO);

  $person = array('email' => $email, 'name' => $name);
  sendgrid_marketing_email_add($list, $person);
}
